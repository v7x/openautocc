Automatically add CC and BCC recipients to your emails in gmail.

Distributed under the MIT license, see LICENSE.txt.

TOS for InboxSDK can be found at https://www.inboxsdk.com/terms
