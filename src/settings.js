/*
 * settings.js handles the extension settings, loading values to the settings
 * page when it opens and saving them when it closes.
 */
let checkcc = document.getElementById('checkcc');
let checkbcc = document.getElementById('checkbcc');
let inputcc = document.getElementById('inputcc');
let inputbcc = document.getElementById('inputbcc');

const DEFAULTS = {checkccval:false, checkbccval:false, inputccval:"", inputbccval:""};

//Get saved settings and display them in the appropriate spots on the page.
function getSettings() {
    chrome.storage.sync.get(DEFAULTS, function(data){
		console.log(data.checkccval);
        checkcc.checked = data.checkccval;
		console.log(data.checkbccval);
        checkbcc.checked = data.checkbccval;
        inputcc.value = data.inputccval;
        inputbcc.value = data.inputbccval;
    });
}

//Save the entered settings. Who'd have guessed?
function saveSettings() {
    var usecc = checkcc.checked;
	console.log(usecc);
    var usebcc = checkbcc.checked;
	console.log(usebcc);
    var cclist = inputcc.value;
    var bcclist = inputbcc.value;
    chrome.storage.sync.set({
        checkccval:usecc,
        checkbccval:usebcc,
        inputccval:cclist,
        inputbccval:bcclist
    }, function(){
        var stat = document.getElementById('stat');
        stat.innerHTML = "Settings saved.";
        setTimeout(function() {
            status.textContent="";
        }, 1000); //1000 is somewhat arbitrary, increase/decrease as seen fit
    });
}

document.addEventListener('DOMContentLoaded', getSettings);
document.getElementById('save').addEventListener('click', saveSettings);
