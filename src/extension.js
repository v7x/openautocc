"use strict";

console.log("Extension loading...");
const jQuery = require("jquery");
const $ = jQuery;
const GmailFactory = require("@v7x/gmail");
const gmail = new GmailFactory.Gmail($);
window.gmail = gmail;

console.log("Loaded.");
gmail.observe.on("load", () => {
    gmail.observe.on("compose", function (compose, composeType) {
        document.dispatchEvent(new CustomEvent('compose', {composed: true}));
        document.addEventListener('autocc', function(data) {
            console.log("Data received.");
            var usecc = data.checkccval;
            var usebcc = data.checkbccval;
            var autocc = data.inputccval;
            var autobcc = data.inputbccval;
            if(usecc) {
                compose.addcc(autocc);
            }

            if(usebcc) {
                compose.addbcc(autobcc);
            }
        });
    });
});
