"use strict";

const DEFAULTS = {checkccval:false, checkbccval:false, inputccval:"", inputbccval:""};

chrome.webNavigation.onCompleted.addListener(function() {
    chrome.storage.sync.get(DEFAULTS, function(keys) {
        console.log(keys);
        document.addEventListener('compose', function (comp) {
            if(comp.composed){
                console.log("Compose detected.");
            }
            document.dispatchEvent(new CustomEvent('autocc', keys));
        });
    });
}, {url: [{hostSuffix : '.mail.google.com', pathPrefix: '/mail/u/0/'}]});
